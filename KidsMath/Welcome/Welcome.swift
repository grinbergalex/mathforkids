//
//  Welcome.swift
//  KidsMath
//
//  Created by Alex Grinberg on 27/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class Welcome: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    
    @IBOutlet weak var startBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        image1.shakeCard()
        image2.shakeCard()
        
        image3.pulsateCard()
        image4.pulsateCard()
        
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func start (_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "InstructionVC", sender: self)
        
    }

}
