//
//  Level1.swift
//  KidsMath
//
//  Created by Alex Grinberg on 25/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class Level1: UIViewController {

    var finalSum: Int!
    var option1: Int!
    var option2: Int!
    var option3: Int!
    var option4: Int!
    
    @IBOutlet weak var sum1: UIImageView!
    @IBOutlet weak var sum2: UIImageView!
    
    @IBOutlet weak var result1: UIImageView!
    @IBOutlet weak var result2: UIImageView!
    @IBOutlet weak var result3: UIImageView!
    @IBOutlet weak var result4: UIImageView!
    

    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var restartBtn: UIButton!
    
    @IBOutlet weak var scoreF:UILabel!
    
    @IBOutlet weak var countDown: UILabel!
    
    var playerScore: Int = 0
    
    var timer = Timer()
    var seconds = 60
    
    var isTimerRunning = false
    var resumeTimer = false
    
    var cardNamesArray: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    var resultArray: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
    var cardSignsArray: [String] = [""]
    var cardArrayImages: [UIImage] = [UIImage(named: "1")!,
                                      UIImage(named: "2")!,
                                      UIImage(named: "3")!,
                                      UIImage(named: "4")!,
                                      UIImage(named: "5")!,
                                      UIImage(named: "6")!,
                                      UIImage(named: "7")!,
                                      UIImage(named: "8")!,
                                      UIImage(named: "9")!,
                                      UIImage(named: "10")!,
                                      ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.sum1.image = UIImage(named: "LMCardBack")
        self.sum2.image = UIImage(named: "LMCardBack")
        restartBtn.isHidden = true
        stopBtn.isHidden = true
        
        countDown.text = "60"
        
        stopBtn.isEnabled = false
        stopBtn.layer.opacity = 0.8
        
        self.playBtn.pulsateButton()
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(firstResult(_:)))
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(secondResult(_:)))
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(thirdResult(_:)))
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(forthResult(_:)))
        
        result1.isUserInteractionEnabled = true
        result1.addGestureRecognizer(tapGestureRecognizer1)
        
        result2.isUserInteractionEnabled = true
        result2.addGestureRecognizer(tapGestureRecognizer2)
        
        result3.isUserInteractionEnabled = true
        result3.addGestureRecognizer(tapGestureRecognizer3)
        
        result4.isUserInteractionEnabled = true
        result4.addGestureRecognizer(tapGestureRecognizer4)
        
        
        
        
    }
    
    @objc func firstResult(_ sender:AnyObject){
        if option1 == finalSum + 1 {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
            if playerScore == 10 {
               stopGame()
            }
            
            setTable()
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    @objc func secondResult(_ sender:AnyObject){
        if option2 == finalSum + 1 {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
            setTable()
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    @objc func thirdResult(_ sender:AnyObject){
        if option3 == finalSum + 1 {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
            setTable()
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    @objc func forthResult(_ sender:AnyObject){
        if option4 == finalSum + 1 {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            setTable()
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    func stopGame() {
        
        playBtn.isEnabled = false
        playBtn.layer.opacity = 1
        stopBtn.isEnabled = false
        timer.invalidate()
        restartBtn.isHidden = true
        
        restartBtn.setTitle("Next Level", for: .normal)
        restartBtn.addTarget(self, action: #selector(nextLevel), for: .touchUpInside)
        
        sum1.image = UIImage(named: "LMCardBack")
        sum2.image = UIImage(named: "LMCardBack")
        
        result1.image = UIImage(named: "LMCardBack")
        result2.image = UIImage(named: "LMCardBack")
        result3.image = UIImage(named: "LMCardBack")
        result4.image = UIImage(named: "LMCardBack")
        
        countDown.text = "Game Over"
        
    }
    
    @objc func nextLevel() {
        
        self.performSegue(withIdentifier: "Level2", sender: self)
        
    }
    
    
    
    @IBAction func stop(_ sender: UIButton) {
        
        playBtn.isEnabled = true
        playBtn.layer.opacity = 1
        stopBtn.isEnabled = false
        timer.invalidate()
        seconds = 60
        restartBtn.isHidden = true
        
        sum1.image = UIImage(named: "LMCardBack")
        sum2.image = UIImage(named: "LMCardBack")
        
        result1.image = UIImage(named: "LMCardBack")
        result2.image = UIImage(named: "LMCardBack")
        result3.image = UIImage(named: "LMCardBack")
        result4.image = UIImage(named: "LMCardBack")

        countDown.text = "Game Over"

        
        
    }
    
    @IBAction func restart(_ sender: UIButton) {
        
        stopBtn.isEnabled = true
        playBtn.isEnabled = false
        //restartBtn.isHidden = true
        
        timer.invalidate()
        setTable()
        runTimer()
        
    }
    
    
    @IBAction func startPlay(_ sender: UIButton) {
        //Randomizing Cards:
        //1st Card:
        
            countDown.text = "\(seconds)"
            playBtn.setTitle("Playing", for: .normal)
            playBtn.isEnabled = false
            playBtn.layer.opacity = 0.5
            seconds = 60
            self.playerScore = 0
            stopBtn.isEnabled = true
            stopBtn.layer.opacity = 1
            restartBtn.isHidden = false
        restartBtn.isEnabled = true
        restartBtn.setTitle("HIT", for: .normal)
        
        setTable()
        runTimer()
        
        
        }

    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        if seconds == 0 {
            countDown.text = "60"
            
        }
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate time's up.
        } else {
            seconds -= 1
            //countDown.text = timeString(time: TimeInterval(seconds))
            countDown.text = String(seconds)
        }
    }

    
    func resultCheck() {
        
        if option1 == finalSum {
            
            playerScore += 1
            print("Score")
            
        }
        
    }
    
    func setTable() {
        
        let firstNumber: Int = Int(arc4random_uniform(9))
        let firstCardString: String = self.cardNamesArray[firstNumber]
        self.sum1.image = UIImage(named: firstCardString)
        
        //2nd Card:
        let secondNumber: Int = Int(arc4random_uniform(9))
        let secondCardString: String = self.cardNamesArray[secondNumber]
        self.sum2.image = UIImage(named: secondCardString)
        
        
        //Randomizing Results:
        //Result 1:
        let result1: Int = Int(arc4random_uniform(20))
        let result1Card: String = self.resultArray[result1]
        self.result1.image = UIImage(named:result1Card)
        self.option1 = result1
        //Result 2:
        let result2: Int = Int(arc4random_uniform(20))
        let result2Card: String = self.resultArray[result2]
        self.result2.image = UIImage(named: result2Card)
        self.option2 = result2
        //Result 3:
        let result3: Int = Int(arc4random_uniform(20))
        let result3Card: String = self.resultArray[result3]
        self.result3.image = UIImage(named: result3Card)
        self.option3 = result3
        //Result 4:
        let result4: Int = Int(arc4random_uniform(20))
        let result4Card: String = self.resultArray[result4]
        self.result4.image = UIImage(named: result4Card)
        self.option4 = result4
        // Setup - an + signs for result. Giving final sum
        let sum = firstNumber + secondNumber
        print(sum)
        self.finalSum = sum
        
    }
    
}
