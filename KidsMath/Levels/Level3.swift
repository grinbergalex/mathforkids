//
//  ViewController.swift
//  KidsMath
//
//  Created by Alex Grinberg on 24/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class Level3: UIViewController {
    
    var finalSum: Int!
    var option1: Int!
    var option2: Int!
    var option3: Int!
    var option4: Int!
    
    @IBOutlet weak var sum1: UIImageView!
    @IBOutlet weak var sum2: UIImageView!
    @IBOutlet weak var sum3: UIImageView!
    
    @IBOutlet weak var result1: UIImageView!
    @IBOutlet weak var result2: UIImageView!
    @IBOutlet weak var result3: UIImageView!
    @IBOutlet weak var result4: UIImageView!
    
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var scoreF:UILabel!
    
    @IBOutlet weak var countDown: UILabel!
    
    var playerScore: Int = 0
    
    var timer = Timer()
    var seconds = 10
    
    var isTimerRunning = false
    var resumeTimer = false
    
    var cardNamesArray: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    var cardSignsArray: [String] = [""]
    var cardArrayImages: [UIImage] = [UIImage(named: "1")!,
                                      UIImage(named: "2")!,
                                      UIImage(named: "3")!,
                                      UIImage(named: "4")!,
                                      UIImage(named: "5")!,
                                      UIImage(named: "6")!,
                                      UIImage(named: "7")!,
                                      UIImage(named: "8")!,
                                      UIImage(named: "9")!,
                                      UIImage(named: "10")!,
                                      ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(firstResult(_:)))
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(secondResult(_:)))
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(thirdResult(_:)))
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(forthResult(_:)))
        
        result1.isUserInteractionEnabled = true
        result1.addGestureRecognizer(tapGestureRecognizer1)
        
        result2.isUserInteractionEnabled = true
        result2.addGestureRecognizer(tapGestureRecognizer2)
        
        result3.isUserInteractionEnabled = true
        result3.addGestureRecognizer(tapGestureRecognizer3)
        
        result4.isUserInteractionEnabled = true
        result4.addGestureRecognizer(tapGestureRecognizer4)
        
     //   sum1.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 70, height: 70)
        


        
    }
    
    @objc func firstResult(_ sender:AnyObject){
        if option1 == finalSum {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }

    }
    
    @objc func secondResult(_ sender:AnyObject){
        if option2 == finalSum {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    @objc func thirdResult(_ sender:AnyObject){
        if option3 == finalSum {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }
    
    @objc func forthResult(_ sender:AnyObject){
        if option4 == finalSum {
            playerScore += 1
            scoreF.text = "\(playerScore)"
            print("Score")
            
        } else {
            playerScore -= 1
            scoreF.text = "\(playerScore)"
        }
        
    }


    
    @IBAction func startPlay(_ sender: UIButton) {
        //Randomizing Cards:
        //1st Card:
        
        sender.tag += 1
        if sender.tag > 2 {
            sender.tag = 0
        }
        
        switch sender.tag {
        case 1:
            
            seconds = 10
            countDown.text = "\(seconds)"
            let firstNumber: Int = Int(arc4random_uniform(9))
            let firstCardString: String = self.cardNamesArray[firstNumber]
            self.sum1.image = UIImage(named: firstCardString)
            
            //2nd Card:
            let secondNumber: Int = Int(arc4random_uniform(9))
            let secondCardString: String = self.cardNamesArray[secondNumber]
            self.sum3.image = UIImage(named: secondCardString)
            
            //Plus Minus sign:
            let mathsign: Int = Int(arc4random_uniform(1))
            let mathSignCard: String = self.cardSignsArray[mathsign]
            self.sum2.image = UIImage(named: mathSignCard)
            
            //Randomizing Results:
            //Result 1:
            let result1: Int = Int(arc4random_uniform(1))
            let result1Card: String = self.cardNamesArray[result1]
            self.result1.image = UIImage(named:result1Card)
            self.option1 = result1
            //Result 2:
            let result2: Int = Int(arc4random_uniform(1))
            let result2Card: String = self.cardNamesArray[result2]
            self.result2.image = UIImage(named: result2Card)
            self.option2 = result2
            //Result 3:
            let result3: Int = Int(arc4random_uniform(1))
            let result3Card: String = self.cardNamesArray[result3]
            self.result3.image = UIImage(named: result3Card)
            self.option3 = result3
            //Result 4:
            let result4: Int = Int(arc4random_uniform(1))
            let result4Card: String = self.cardNamesArray[result4]
            self.result4.image = UIImage(named: result4Card)
            self.option4 = result4
            
            if isTimerRunning == false {
                runTimer()
            }
            
            // Setup - an + signs for result. Giving final sum
            if mathsign == 0 {
                
                let sum = firstNumber + secondNumber
                print(sum)
                self.finalSum = sum
                
            } else {
                
                let sum = firstNumber - secondNumber
                print(sum)
                self.finalSum = sum
                
            }
        case 2:
            let firstNumber: Int = Int(arc4random_uniform(9))
            let firstCardString: String = self.cardNamesArray[firstNumber]
            self.sum1.image = UIImage(named: firstCardString)
            
            //2nd Card:
            let secondNumber: Int = Int(arc4random_uniform(9))
            let secondCardString: String = self.cardNamesArray[secondNumber]
            self.sum3.image = UIImage(named: secondCardString)
            
            //Plus Minus sign:
            let mathsign: Int = Int(arc4random_uniform(1))
            let mathSignCard: String = self.cardSignsArray[mathsign]
            self.sum2.image = UIImage(named: mathSignCard)
            
            //Randomizing Results:
            //Result 1:
            let result1: Int = Int(arc4random_uniform(1))
            let result1Card: String = self.cardNamesArray[result1]
            self.result1.image = UIImage(named:result1Card)
            self.option1 = result1
            //Result 2:
            let result2: Int = Int(arc4random_uniform(1))
            let result2Card: String = self.cardNamesArray[result2]
            self.result2.image = UIImage(named: result2Card)
            self.option2 = result2
            //Result 3:
            let result3: Int = Int(arc4random_uniform(1))
            let result3Card: String = self.cardNamesArray[result3]
            self.result3.image = UIImage(named: result3Card)
            self.option3 = result3
            //Result 4:
            let result4: Int = Int(arc4random_uniform(1))
            let result4Card: String = self.cardNamesArray[result4]
            self.result4.image = UIImage(named: result4Card)
            self.option4 = result4
            
            if isTimerRunning == false {
                runTimer()
            }
            
            // Setup - an + signs for result. Giving final sum
            if mathsign == 0 {
                
                let sum = firstNumber + secondNumber
                print(sum)
                self.finalSum = sum
                
            } else {
                
                let sum = firstNumber - secondNumber
                print(sum)
                self.finalSum = sum
                
            }
        default:
            break
        }

        
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate time's up.
        } else {
            seconds -= 1
            //countDown.text = timeString(time: TimeInterval(seconds))
            countDown.text = String(seconds)
        }
    }
    
    func resultCheck() {
        
        if option1 == finalSum {
            
            playerScore += 1
            print("Score")
            
        }
    
    }

}

